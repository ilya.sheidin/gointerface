package main

import (
	"fmt"
	"io"
	"os"
	"reflect"
)

type shape interface {
	io.Writer
	io.Reader
	getArea() float64
	readFile() string
}

func main() {
	if Contains(os.Args[1:], "triangle") || Contains(os.Args[1:], "square") {
		if os.Args[1] == "triangle" {
			t := triangle{
				height: 10,
				base:   15,
			}
			saveToFile(t)
		} else {
			s := square{sideLength: 7}
			saveToFile(s)
		}
	} else {
		fmt.Println("Missing shape type in argument")
	}
}

func saveToFile(s shape) {
	bs := []byte("The area of " + reflect.TypeOf(s).Name() + " is: " + fmt.Sprintf("%f", s.getArea()))
	_, err := s.Write(bs)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
	s.readFile()
}

func Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

type square struct {
	sideLength float64
}

func (s square) Write(bs []byte) (n int, err error) {
	fileName := "my_square_file.txt"
	f, err := os.Create(fileName)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
	_, err = f.Write(bs)
	if err != nil {
		fmt.Println("Failed to write file", err)
		os.Exit(1)
	} else {
		fmt.Println(fileName, "saved")
	}
	
	return len(bs), nil
}

func (s square) getArea() float64 {
	return s.sideLength * s.sideLength
}

func (s square) readFile() string {
	fileContent, err := ioutil.ReadFile("my_square_file.txt")
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
	_, err = s.Read(fileContent)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
	return string(fileContent)
}

func (s square) Read(p []byte) (n int, err error) {
	
	fmt.Println(string(p))
	
	return len(p), nil
}

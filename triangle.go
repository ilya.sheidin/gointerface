package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

type triangle struct {
	height float64
	base   float64
}

func (t triangle) getArea() float64 {
	return 0.5 * t.base * t.height
}

func (t triangle) Write(bs []byte) (n int, err error) {
	fileName := "my_triangle_file.txt"
	f, err := os.Create(fileName)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
	_, err = f.Write(bs)
	if err != nil {
		fmt.Println("Failed to write file", err)
		os.Exit(1)
	} else {
		fmt.Println(fileName, "saved")
	}
	
	return len(bs), nil
}

func (t triangle) readFile() string {
	fileContent, err := ioutil.ReadFile("my_square_file.txt")
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
	_, err = t.Read(fileContent)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
	return string(fileContent)
}

func (t triangle) Read(p []byte) (n int, err error) {
	
	fmt.Println(string(p))
	
	return len(p), nil
}
